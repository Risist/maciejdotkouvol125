﻿using UnityEngine;
using System.Collections;

public class RemoveAfterDelay : MonoBehaviour
{
    public Timer timer;
	//float timeLeft;

	private void Start()
	{
		timer.Restart();
	}

	void Update()
    {
        if (timer.IsReady())
            Destroy(gameObject);
    }
}
