﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BulletController : MonoBehaviour {

    [SerializeField] Vector2 movementSpeed;
    [SerializeField] Timer tDestroyAfterHit;

    /// References to components
    Rigidbody2D body;

    /// Flow control
    bool isFlying;

	void Start () {
        body = GetComponent<Rigidbody2D>();
        isFlying = true;
        Debug.Assert(CameraController.instance);
        CameraController.instance.SetCurrentBullet(transform);
	}

    private void FixedUpdate()
    {
        if (isFlying)
        {
            body.AddForce(body.transform.TransformVector(movementSpeed));
        }
        else if(tDestroyAfterHit.IsReady())
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isFlying = false;
        tDestroyAfterHit.Restart();
    }
}
