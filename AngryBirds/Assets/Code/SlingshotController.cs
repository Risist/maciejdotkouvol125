﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingshotController : MonoBehaviour {

    [SerializeField] GameObject bulletPrefab;
    Vector2 initialPosition;

    [Space]
    [SerializeField, Range(0f, 1f)] float toInitialLerpScale;

    [SerializeField, Range(0f, 1f)] float toMouseLerpScale;
    [SerializeField]                float maxDistance; // max distance from initial position

    /// Flow Control
    bool onDrag;

    void Start ()
    {
        initialPosition = transform.position;
	}

    void FixedUpdate()
    {
        if (onDrag)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var v = Vector2.Lerp(transform.position, mousePosition, toMouseLerpScale);

            var vWer = (v - initialPosition);
            if (vWer.sqrMagnitude > maxDistance * maxDistance)
                transform.position = initialPosition + (v - initialPosition).normalized * maxDistance;
            else
                transform.position = v;
        }
        else
            transform.position = Vector2.Lerp(transform.position, initialPosition, toInitialLerpScale);
    }
    
    private void OnMouseDrag()
    {
        onDrag = true;
        
    }
    private void OnMouseUp()
    {
        onDrag = false;
        Vector2 toInitial = (Vector2)transform.position - initialPosition;
        SpawnBullet(transform.position, Vector2.SignedAngle(Vector2.right, -toInitial));
    }

    void SpawnBullet(Vector2 position, float rotation)
    {
        Debug.Assert(CameraController.instance);

        var obj = Instantiate(bulletPrefab, position, Quaternion.Euler(0, 0, rotation));
        CameraController.instance.SetCurrentBullet(obj.transform);
    }


}
