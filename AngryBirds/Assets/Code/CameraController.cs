﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public static CameraController instance;

    [SerializeField, Range(0f, 1f)] float toBulletLerpScale;
    Transform currentBullet;
    public void SetCurrentBullet(Transform currentBullet)
    {
        this.currentBullet = currentBullet;
    }
    public bool HasCurrentBullet()
    {
        return currentBullet;
    }

    [SerializeField, Range(0f, 1f)] float toInitialLerpScale;
    Vector2 initialPosition;


    void Awake () { instance = this; }
    void Start ()
    {
        initialPosition = transform.position;
	}
	
	void Update ()
    {
        Vector3 newPosition;
		if(currentBullet)
            newPosition = Vector2.Lerp(transform.position, currentBullet.position, toBulletLerpScale);
        else
            newPosition = Vector2.Lerp(transform.position, initialPosition, toInitialLerpScale);
        newPosition.z = transform.position.z;
        transform.position = newPosition;
	}
}
