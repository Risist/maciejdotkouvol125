﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour {

    public GameObject prefab;
    GameObject[] objects;
    private void Start()
    {
        objects = new GameObject[250];
        for(int i = 0; i < 250; ++i)
        {
            objects[i] = Instantiate(prefab);
            objects[i].SetActive(false);
        }
    }
    public GameObject GetUnusedObject()
    {
        foreach(var it in objects)
            if(!it.activeInHierarchy)
            {
                it.SetActive(true);
                return it;
            }
        return null;
    }
}
