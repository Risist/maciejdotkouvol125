﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float normalGravity;
    public float increasedGravity;

    /// References
    PointEffector2D pointEffector;

    #region Connecting mass
    [SerializeField] float splitForce;

    int massCount = 1;
    #endregion Connecting mass

    void Start()
    {
        pointEffector = GetComponentInChildren<PointEffector2D>();

        massCount = 1;
    }
    private void FixedUpdate()
    {
        Debug.Assert(GameManager.instance);

        pointEffector.forceMagnitude = GameManager.instance.ballsCount >= GameManager.instance.maxBalls ?
            increasedGravity : normalGravity;
        increasedSizeThisFrame = false;
    }
    private void OnEnable()
    {
        if(GameManager.instance)
            GameManager.instance.ballsCount++;

        var colliders = GetComponentsInChildren<Collider2D>();
        foreach (var it in colliders)
            it.enabled = true;
        massCount = 1;
        transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }
    private void OnDisable()
    {
        if(GameManager.instance)
            GameManager.instance.ballsCount--;
    }

    bool increasedSizeThisFrame;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        var other = collision.gameObject.GetComponent<BulletController>();
        if (!other || other.increasedSizeThisFrame)
            return;
        if (GameManager.instance.ballsCount >= GameManager.instance.maxBalls)
            return;

        IncreaseBall(other);
        if (massCount >= 50)
            SplitBullet(0, transform.localScale.x);
        
    }

    void IncreaseBall(BulletController other)
    {
        massCount = massCount + other.massCount;
        //Debug.Log("incr + " + massCount);

        float scaleSum = transform.localScale.x + other.transform.localScale.x;
        transform.position =
            transform.position * (transform.localScale.x / scaleSum) +
            other.transform.position * (other.transform.localScale.x / scaleSum);

        float newR = Mathf.Sqrt(transform.localScale.x * transform.localScale.x +
            other.transform.localScale.x * other.transform.localScale.x);
        transform.localScale = new Vector3(newR, newR, 1f);

        other.gameObject.SetActive(false);
        increasedSizeThisFrame = true;
    }
    
    void SplitBullet(float randomRadiusMin, float randomRadiusMax)
    {

        for (int i = 0; i < massCount; ++i)
        {
            var ball = GameManager.instance.pool.GetUnusedObject();
            ball.transform.position = (Vector2)transform.position + Random.insideUnitCircle * Random.Range(randomRadiusMin, randomRadiusMax);
            ball.GetComponent<Rigidbody2D>().AddForce(Quaternion.Euler(0,0,Random.value*360) * Vector2.up * splitForce);
            
            GameManager.instance.StartCoroutine(TurnOffCollsion(ball));
        }
        gameObject.SetActive(false);
    }

    IEnumerator TurnOffCollsion(GameObject obj)
    {
        var colliders = obj.GetComponentsInChildren<Collider2D>();
        foreach (var it in colliders)
            it.enabled = false;
        yield return new WaitForSeconds(0.5f);
        foreach (var it in colliders)
            it.enabled = true;
    }
}
