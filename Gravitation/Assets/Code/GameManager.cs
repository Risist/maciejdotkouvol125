﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PoolManager))]
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int maxBalls = 250;
    [HideInInspector] public PoolManager pool;

    [System.NonSerialized] public int ballsCount;
    [SerializeField] float spawnRadiusMax;
    [SerializeField, Space] Text textBallsCount; 
    

    void Awake() {instance = this; }
    void Start()
    {
        pool = GetComponent<PoolManager>();
        StartCoroutine(SpawnBullets());
    }

    IEnumerator SpawnBullets()
    {
        yield return new WaitForSeconds(1/4f);
        SpawnBall();
        UpdateText();

        if (ballsCount < maxBalls)
            StartCoroutine(SpawnBullets());
    }

    public void SpawnBall()
    {
        var obj = pool.GetUnusedObject();
        if(obj)
        {
            obj.transform.position = (Vector2)transform.position + Random.insideUnitCircle * Random.Range(0, spawnRadiusMax);
            obj.transform.rotation = Quaternion.Euler(0, 0, Random.value * 360f);
        }
    }
    public void UpdateText()
    {
        textBallsCount.text = "Balls count = " + ballsCount + " \\ " + maxBalls;
    }
}


